--Using the database PC_DBT_DB with AccountAdmin role for full access
Use database PC_DBT_DB ; 
USE ROLE ACCOUNTADMIN ; 

--Creating the target table
create table contacts_csv
(
ID NUMBER,
lastname VARCHAR(50),
firstname VARCHAR(50),
company VARCHAR(50),
email VARCHAR(100),
workphone VARCHAR(50),
cellphone VARCHAR(50),
streetaddress VARCHAR(50),
city VARCHAR(50),
postalcode VARCHAR(50)
);

--Creating the file format of CSV
CREATE FILE FORMAT Demo_Contacts_CSV 
TYPE = 'CSV' 
COMPRESSION = 'AUTO' 
FIELD_DELIMITER = '|' 
RECORD_DELIMITER = '\n' 
SKIP_HEADER = 1 
FIELD_OPTIONALLY_ENCLOSED_BY = 'NONE' 
TRIM_SPACE = FALSE 
ERROR_ON_COLUMN_COUNT_MISMATCH = FALSE 
ESCAPE = 'NONE' 
ESCAPE_UNENCLOSED_FIELD = '\134' 
DATE_FORMAT = 'AUTO' 
TIMESTAMP_FORMAT = 'AUTO' 
NULL_IF = ('') 
COMMENT = 'File format for Contacts.csv data ';

--Creating the Stages
CREATE OR REPLACE STAGE stg_contact
  URL='s3://edm-hackathon-28082023/snowflake-uploads/'
  CREDENTIALS=(AWS_KEY_ID='AKIAYZ75EHXOTG5TIILY' AWS_SECRET_KEY='EmiUFzhKq6aDUfCdSzDOsw8OjlgKozPLYRmR0YDb');
--List down the external stage that we used 
list @stg_contact ;

--Temproray to move the data from external stage to target table
Copy into contacts_csv from @stg_contact
--pattern = '.*.csv.*'
file_format=Demo_Contacts_CSV ; 

--retrieve the data present in stage
SELECT $1 , $2 from @stg_contact ;

SELECT * from contacts_csv ; 

--Continous data load from AWS S3 bucket
CREATE or REPLACE PIPE Contact_Snowpipe1
  auto_ingest = true
  as
  COPY INTO contacts_csv
  FROM @stg_contact
  file_format = Demo_Contacts_CSV;

