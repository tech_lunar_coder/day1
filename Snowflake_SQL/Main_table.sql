create schema PC_DBT_DB.Main;

 
# creating a main table 
create table PC_DBT_DB.Main.Contacts
(
ID NUMBER,
Name VARCHAR(50),
company VARCHAR(50),
email VARCHAR(100),
workphone VARCHAR(50),
cellphone VARCHAR(50),
Address VARCHAR(100),
postalcode VARCHAR(50),
FirstRegisteredDttm TIMESTAMP Default current_timestamp,
LastChangedDttm TIMESTAMP default current_timestamp,
ActiveInd VARCHAR(2),
SoftDeleteInd VARCHAR(2) default 'N'
);

 

 

create or replace stream PC_DBT_DB.Main.Contacts_CDC on table PC_DBT_DB.PUBLIC.contacts_csv;

create or replace task PC_DBT_DB.Main.ETL_Contacts
    warehouse = COMPUTE_WH
    schedule = '1 minute'
when
    system$stream_has_data('PC_DBT_DB.Main.Contacts_CDC')
as
    merge into PC_DBT_DB.Main.Contacts contacts using PC_DBT_DB.Main.Contacts_CDC stream_contacts on
    contacts.ID = stream_contacts.ID
    when matched
        and stream_contacts.METADATA$ACTION = 'INSERT'
        and stream_contacts.METADATA$ISUPDATE = 'TRUE'
        then
        insert(ID, 
        name, 
        company, 
        email, 
        workphone, 
        cellphone, 
        address,
        postalcode, 
        FirstRegisteredDttm,
        LastChangedDttm,
        ActiveInd)
        values(stream_contacts.ID, 
        stream_contacts.name, 
        stream_contacts.company, 
        stream_contacts.email, 
        stream_contacts.workphone, 
        stream_contacts.cellphone, 
        stream_contacts.address,
        stream_contacts.postalcode,
        contacts.FirstRegisteredDttm,
        current_timestamp(),
        'Y')
    when matched
        and stream_contacts.METADATA$ACTION = 'DELETE'
        and stream_contacts.METADATA$ISUPDATE = 'TRUE'
        then update set
            contacts.ActiveInd = 'N',
            contacts.LastChangedDttm = current_timestamp(),
            contacts.SoftDeleteInd = 'Y'
    when matched
        and stream_contacts.METADATA$ACTION = 'DELETE'
        and stream_contacts.METADATA$ISUPDATE = 'FALSE'
        then update set
            contacts.ActiveInd = 'N',
            contacts.LastChangedDttm = current_timestamp()
    when not matched
        and stream_contacts.METADATA$ACTION = 'INSERT'
        and stream_contacts.METADATA$ISUPDATE = 'FALSE'
        then 
        insert(ID, 
        name, 
        company, 
        email, 
        workphone, 
        cellphone, 
        address,
        postalcode, 
        ActiveInd)
        values(stream_contacts.ID, 
        stream_contacts.name, 
        stream_contacts.company, 
        stream_contacts.email, 
        stream_contacts.workphone, 
        stream_contacts.cellphone, 
        stream_contacts.address,
        stream_contacts.postalcode, 
        'Y');

